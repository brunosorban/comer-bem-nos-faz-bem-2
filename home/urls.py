from django.urls import include, path

from . import views

# app_name = 'home'
urlpatterns = [
    path('about/', views.about, name='about'),
    path('', views.index, name='index'),
]
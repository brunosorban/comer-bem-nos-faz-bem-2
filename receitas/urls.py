from django.urls import path

from . import views

app_name = 'receitas'
urlpatterns = [
    path('', views.PostListView.as_view(), name='index_receitas'),
    path('create/', views.PostCreateView.as_view(), name='create'),
    path('update/<int:pk>/', views.PostUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', views.PostDeleteView.as_view(), name='delete'),
    path('<int:pk>/', views.PostDetailView.as_view(), name='detail'),
    path('<int:pk>/comment/', views.create_review, name='comment'),
    path('category/', views.CategoryListView.as_view(), name='category_index'),
    path('category/<int:pk>/', views.CategoryDetailView.as_view(), name='detail_category'),
    path('category/create', views.CategoryCreateView.as_view(), name='create-category'),
]
from django.views import generic
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.shortcuts import render, get_object_or_404
from .models import Post, Comment, Category
from .forms import CommentForm

class PostListView(generic.ListView):
    model = Post
    template_name = 'receitas/index.html'

class PostDetailView(generic.DetailView):
    model = Post
    template_name = 'receitas/detail.html'

class PostCreateView(generic.CreateView):
    model = Post
    template_name = 'receitas/create.html'
    fields = ['name', 'ingredients', 'preparation', 'poster_url']

    def get_success_url(self):
        return reverse('receitas:detail', args=(self.object.id, ))

class PostUpdateView(generic.UpdateView):
    model = Post
    template_name = 'receitas/update.html'
    fields = ['name', 'ingredients', 'preparation', 'poster_url']

    def get_success_url(self):
        return reverse('receitas:detail', args=(self.object.id, ))

class PostDeleteView(generic.DeleteView):
    model = Post
    template_name = 'receitas/delete.html'
    success_url = reverse_lazy('receitas:index_receitas')

def create_review(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            review_author = form.cleaned_data['author']
            review_text = form.cleaned_data['text']
            review = Comment(author=review_author,
                            text=review_text,
                            post=post)
            review.save()
            return HttpResponseRedirect(
                reverse('receitas:detail', args=(pk, )))
    else:
        form = CommentForm()
    context = {'form': form, 'post': post}
    return render(request, 'receitas/comment.html', context)

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'receitas/category_index.html'

class CategoryDetailView(generic.DetailView):
    model = Category
    template_name = 'receitas/category.html'

class CategoryCreateView(generic.CreateView):
    model = Category
    template_name = 'receitas/create_category.html'
    fields = ['name', 'author', 'posts']
    success_url = reverse_lazy('receitas:category_index')